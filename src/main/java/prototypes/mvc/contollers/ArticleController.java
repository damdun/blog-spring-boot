package prototypes.mvc.contollers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.View;
import org.springframework.web.servlet.view.RedirectView;
import prototypes.db.ArticlesDatabase;
import prototypes.mvc.models.Article;

import java.net.URI;
import java.util.HashMap;
import java.util.Map;

import static java.sql.DriverManager.getConnection;

@Controller
public class ArticleController {

    private ArticlesDatabase database;

    @Autowired
    public ArticleController(@Value("${database.driver}") String databaseDriver,
                             @Value("${database.url}") String databaseUrl) {
        try {
            Class.forName(databaseDriver);
            database = new ArticlesDatabase(getConnection(databaseUrl));
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ModelAndView showList() {
        return new ModelAndView("list", "articles", database.getArticles());
    }

    @RequestMapping(value = "/create", method = RequestMethod.GET)
    public ModelAndView initCreate() {
        Map<String, Object> model = new HashMap<>();
        model.put("formAction", "/create");
        model.put("article", new Article());

        return new ModelAndView("creator", model);
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public View doCreate(@RequestParam("title") String title, @RequestParam("text") String text) {
        database.insertArticle(new Article(title, text));
        return new RedirectView(URI.create("/").toString());
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ModelAndView showDetails(@PathVariable("id") Long id) {
        return new ModelAndView("details", "article", database.findArticle(id));
    }

    @RequestMapping(value = "/{id}/edit", method = RequestMethod.GET)
    public ModelAndView initEdit(@PathVariable("id") Long id) {
        Map<String, Object> model = new HashMap<>();
        model.put("formAction", "/" + id + "/edit");
        model.put("article", database.findArticle(id));
        return new ModelAndView("editor", model);
    }

    @RequestMapping(value = "/{id}/edit", method = RequestMethod.POST)
    public View doEdit(@PathVariable("id") Long id, @RequestParam("title") String title, @RequestParam("text") String text) {
        database.updateArticle(id, new Article(title, text));
        return new RedirectView(URI.create("/").toString());
    }

    @RequestMapping(value = "/{id}/delete", method = RequestMethod.DELETE)
    public ModelAndView doDelete(@PathVariable("id") Long id) {
        return null;
    }

}
