package prototypes;

import com.github.jknack.handlebars.springmvc.HandlebarsViewResolver;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Bean
    public HandlebarsViewResolver viewResolver(@Value("${spring.handlebars.prefix}") String prefix,
                                               @Value("${spring.handlebars.cache}") boolean cache) {
        HandlebarsViewResolver resolver = new HandlebarsViewResolver();
        resolver.setPrefix(prefix);
        resolver.setCache(cache);
        return resolver;
    }
}
